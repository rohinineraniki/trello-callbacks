const fs = require("fs");
const path = require("path");

const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

function callback5() {
  const board_id_path = path.join(__dirname, "./boards.json");
  fs.readFile(board_id_path, "utf-8", function (err, data) {
    if (err) {
      console.error(err);
    } else {
      const parsed_data = JSON.parse(data);
      const get_id = parsed_data.filter((each) => {
        if (each.name === "Thanos") {
          return each;
        }
      });
      const thanos_id = get_id[0].id;
      setTimeout(() => {
        const boards_data = callback1(thanos_id, (err, data) => {
          if (err) {
            console.error(err);
          } else {
            console.log(data);
            const id = data[0].id;
            const list_data = callback2(id, (err, data) => {
              if (err) {
                console.error(err);
              } else {
                console.log(data);
                const filter_list_name = data[id].filter((each) => {
                  if (
                    each.name.toLowerCase() === "mind" ||
                    each.name.toLowerCase() === "space"
                  ) {
                    return each;
                  }
                });

                for (let each of filter_list_name) {
                  callback3(each.id, (err, data) => {
                    if (err) {
                      console.error(err);
                    } else {
                      console.log(data);
                    }
                  });
                }
              }
            });
          }
        });
      }, 2 * 1000);
    }
  });
}

module.exports = callback5;
