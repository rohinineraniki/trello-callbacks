/* 
	Problem 1: 
    Write a function that will return a particular board's information based on the boardID that is passed from the 
    given list of boards in boards.json and then pass control back to the code that called it by using a callback 
    function.
*/
const fs = require("fs");
const path = require("path");
const boards_path = path.join(__dirname, "./boards.json");

function callback1(id, callbackFn) {
  setTimeout(() => {
    fs.readFile(boards_path, "utf-8", function (err, data) {
      if (err) {
        console.error(err);
      } else {
        const parsed_data = JSON.parse(data);
        const filter_by_id = parsed_data.filter((each) => {
          if (id === each.id) {
            return each;
          }
        });
        callbackFn(null, filter_by_id);
      }
    });
  }, 2 * 1000);
}

module.exports = callback1;
