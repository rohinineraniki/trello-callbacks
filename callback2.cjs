const fs = require("fs");
const path = require("path");
const boards_path = path.join(__dirname, "./lists.json");

function callback2(id, callbackFn) {
  setTimeout(() => {
    fs.readFile(boards_path, "utf-8", function (err, data) {
      if (err) {
        console.error(err);
      } else {
        const parsed_data = JSON.parse(data);
        const filter_by_id = Object.keys(parsed_data).reduce((acc, each) => {
          if (id === each) {
            acc[each] ?? (acc[each] = {});
            acc[each] = parsed_data[each];
          }
          return acc;
        }, {});
        callbackFn(null, filter_by_id);
      }
    });
  }, 2 * 1000);
}

module.exports = callback2;
